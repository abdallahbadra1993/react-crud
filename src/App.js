import './App.css';
import Admins from "./container/Admins";
import Charts from "./container/charts/ProfitCharts";
import IncomesCharts from "./container/charts/IncomesCharts"
import ExpensesCharts from "./container/charts/ExpensesCharts";
function App() {
  return (
    <div className="App">
      {/* <Admins /> */}
      <Charts />
      <IncomesCharts/>
      <ExpensesCharts/>
    </div>
  );
}

export default App;
