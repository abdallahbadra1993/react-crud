import React, { useState, useEffect } from "react";
import { Table, Button } from "reactstrap";
import axios from "axios";
import AddAdmin from "./AddAdmin";
import EditAdmin from "./EditAdmin";
export default function Admin() {
  const [state, updateState] = useState({
    Admins: [],
    newAdminsData: {
      username: "",
      password: "",
      profile_picture: "",
    },
    isLoading: false,
    status: "",
    newAdminModal: false,
    editAdminData: {
      admin_id: "",
      username: "",
      password: "",
      profile_picture: "",
    },
    editAdminModal: false,
    noDataFound: "",
  });
  const setState = (nextState) => {
    updateState((prevState) => ({
      ...prevState,
      ...nextState,
    }));
  };
    const request = async () => {
      await axios
        .get("http://localhost:8000/api/getalladmins")
        .then((response) => {
          setState({ Admins: response.data });
        });
    };
  useEffect(() => {

    request();
  }, []);

  const toggleNewAdminModal = () => {
    setState({
      newAdminModal: !state.newAdminModal,
    });
  };
  const onChangeAddAdminHandler = (e) => {
    let  newAdminsData  = state.newAdminsData;
    newAdminsData[e.target.name] = e.target.value;
    setState({ newAdminsData });
  };
    const onChangefilehandle = (e) => {
      let newAdminsData = state.newAdminsData;
      newAdminsData[e.target.name] = e.target.files[0];
      setState({ newAdminsData });
    };

  const add = () => {
    const formData = new FormData();
    formData.append("username", state.newAdminsData.username);
    formData.append("password", state.newAdminsData.password);
    formData.append("profile_picture", state.newAdminsData.profile_picture);
    //console.log(state.newAdminsData.username);

    axios
      .post("http://localhost:8000/api/createadmin", formData, {
        headers: {
          "Accept": "application/json",
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        console.log(res);
        console.log(res.data);
        setState({ newAdminModal: false });
        request();

        //setState({Admins:state.Admins.push(res.data)})
        //setState({ newAdminsData: res.data });
        // const { Admins } = state;
        // const newAdmins = [...Admins];
        // newAdmins.push(res.data);
        // setState(
        //   {
        //     Admins: newAdmins,
        //     newStudentModal: false,
        //     newAdminsData: {
        //       username: "",
        //       password: "",
        //       profile_picture: "",
        //     },
        //   },
        //   () => request()
        // );
  });
}
  const removeAdmin = (admin_id) => {
    console.log(admin_id);
    axios
      .delete(`http://localhost:8000/api/deleteadmin/${admin_id}`)
      .then((res) => {
        console.log(res);
        console.log(res.data);
      });
    // console.log(Admins);
    alert("are sure you want to remove");
    setState({
      Admins: state.Admins.filter(function (obj) {
        return obj.admin_id !== admin_id;
      }),
    });
  };
const toggleEditAdminModal = () => {
  setState({
    editAdminModal: !state.editAdminModal,
  });
};

const onChangeEditAdminHanler = (e) => {
  let { editAdminData } = state;
  editAdminData[e.target.name] = e.target.value;
  setState({ editAdminData });
};

const editAdmin = (admin_id, username, password, profile_picture) => {
  setState({
    editAdminData: { admin_id, username, password, profile_picture },
    editAdminModal: !state.editAdminModal,
  });
};
  const renderHeader = () => {
    let headerElement = ["admin_id", "username", "password", "profile_picture"];

    return headerElement.map((key, index) => {
      return <th key={index}>{key.toUpperCase()}</th>;
    });
  };

  const renderBody = () => {
    return state.Admins.map((admin) => (
      <tr key={admin.admin_id}>
        <td>{admin.admin_id}</td>
        <td>{admin.username}</td>
        <td>{admin.password}</td>
        <td>
          <img
            src={`http://localhost:8000/storage/uploads/${admin.profile_picture}`}
            alt="Italian Trulli"
            width="100px"
            height="100px"
          ></img>
        </td>
        <td className="opration">
          <Button
            color="success"
            className="mr-3"
            size="sm"
            onClick={() =>
              editAdmin(
                admin.admin_id,
                admin.username,
                admin.password,
                admin.profile_picture
              )
            }
          >
            Edit
          </Button>

          <Button
            color="danger"
            size="sm"
            onClick={() => removeAdmin(admin.admin_id)}
          >
            Delete
          </Button>
        </td>
      </tr>
    ));
  };
  return (
    <div className="App">
      <AddAdmin
        toggleNewAdminModal={toggleNewAdminModal}
        newAdminModal={state.newAdminModal}
        onChangeAddAdminHandler={onChangeAddAdminHandler}
        onChangefilehandle={onChangefilehandle}
        add={add}
        newAdminsData={state.newAdminsData}
      />
      <EditAdmin
        toggleEditAdminModal={toggleEditAdminModal}
        editAdminModal={state.editAdminModal}
        onChangeEditAdminHanler={onChangeEditAdminHanler}
        editAdmin={editAdmin}
        editAdminData={state.editAdminData}
        //updateStudent={this.updateStudent}
      />
      <Table id="employee">
        <thead>
          <tr>{renderHeader()}</tr>
        </thead>
        <tbody>{renderBody()}</tbody>
      </Table>
    </div>
  );
}
