import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

export default function EditAdmin (props)  {
  
    return (
      <div>
        <Modal
          isOpen={props.editAdminModal}
          toggle={props.toggleEditAdminModal}
        >
          <ModalHeader toggle={props.toggleEditAdminModal}>
            Update Admin
          </ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="username">username</Label>
              <Input
                id="username"
                name="username"
                value={props.editAdminData.username}
                onChange={props.onChangeEditAdminHanler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password">password</Label>
              <Input
                type="password"
                id="password"
                name="password"
                value={props.editAdminData.password}
                onChange={props.onChangeEditAdminHanler}
              />
            </FormGroup>

            <FormGroup>
              <Label for="profile_picture">profile_picture</Label>
              <Input
                type="file"
                id="profile_picture"
                name="profile_picture"
                //value={this.props.editStudentData.email}
                onChange={props.onChangeEditAdminHanler}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={props.updateStudent}>
              Update
            </Button>
            <Button color="secondary" onClick={props.toggleEditAdminModal}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  
}
